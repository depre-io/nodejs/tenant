(function () {
  'use strict';
  const assign = require('object-assign');
  const defaults = {
      tenantHeader: "x-tenant",
      defaultTenant: "DEFAULT",
      otapHeader: "x-dtap",
      defaultDTAP: "p" 
    };

  function middlewareWrapper(o) {
    // if options are static (either via defaults or custom options passed in), wrap in a function
    let optionsCallback = null;
    if (typeof o === 'function') {
      optionsCallback = o;
    } else {
      optionsCallback = function (req, cb) {
        cb(null, o);
      };
    }

    return function tenantMiddleware(req, res, next) {
      optionsCallback(req, function (err, options) {
        if (err) {
          next(err);
        } else {
          const o = assign({}, defaults, options);

          const hostNameParts = (req.headers['host'] || '').split('.')
            let tenantFromHostName = ''
            switch (hostNameParts.length) {
                case 1:
                case 2:
                    tenantFromHostName = hostNameParts[0]
                    break
                default:
                    tenantFromHostName = hostNameParts[hostNameParts.length - 3]
                    break
            }
          
          let tenant = req.headers[o.tenantHeader] || tenantFromHostName.toLowerCase();
          let otap = req.headers[o.otapHeader] ? '-' + (req.headers[o.otapHeader] || o.defaultDTAP) : '';
          req.tenant = tenant + otap;
          next();
          }
       })
    };
  }
  module.exports = middlewareWrapper;
}());
